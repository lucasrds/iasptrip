(function () {
    'use strict';

    angular
        .module('app')
        .directive('leftMenu', LeftMenu);


    /** @ngInject */
    function LeftMenu() {

        function LeftMenuController($timeout) {
            var vm = this;

            function updateActive(items) {
                return items.map(item => ({
                    ...item, active: window.location.href.includes(item.link)
                }));
            }

            vm.clickLink = function () {
                $timeout(() => { vm.items = updateActive(vm.items); })
            }

            function init() {
                let items = [{
                    title: "Clientes",
                    active: true,
                    link: "/clientes",
                    icon: "fas fa-user",
                },
                {
                    title: "Aviões",
                    active: false,
                    link: "/avioes",
                    icon: "fas fa-plane",
                },
                {
                    title: "Aeroportos",
                    active: false,
                    link: "/aeroportos",
                    icon: "fas fa-building",
                },
                {
                    title: "Voos",
                    active: false,
                    link: "/voos",
                    icon: "fab fa-avianex",
                },
                {
                    title: "Rotas",
                    active: false,
                    link: "/rotas",
                    icon: "fas fa-code-branch",
                },
                {
                    title: "Vendas",
                    active: false,
                    link: "/vendas",
                    icon: "fas fa-shopping-basket",
                },
                {
                    title: "Config",
                    active: false,
                    link: "/config",
                    icon: "fas fa-cogs",
                }];

                vm.items = updateActive(items);
            }
            init();
        }

        function link() {

        }

        return {
            bindToController: true,
            templateUrl: '/view/left-menu.html',
            controller: LeftMenuController,
            controllerAs: 'leftMenu',
            link: link,
            replace: true,
            restrict: 'AE',
            scope: {},
        }
    }

}());