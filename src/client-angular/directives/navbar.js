(function () {
    'use strict';

    angular
        .module('app')
        .directive('navbar', Navbar);


    /** @ngInject */
    function Navbar() {

        function NavbarController($state, $cookies) {
            var vm = this;

            init();

            vm.loggedUser = $cookies.getObject("user");
            
            vm.logout = function () {
                $state.go('adminLogin')
            }

            function init() {}
        }

        function link() {

        }

        return {
            bindToController: true,
            templateUrl: './view/navbar.html',
            controller: NavbarController,
            controllerAs: 'navBar',
            link: link,
            replace: true,
            restrict: 'AE',
            scope: {},
        }
    }

}());