(function () {
    'use strict';

    angular
        .module('app')
        .service('$cart', Cart)

    /** @ngInject */
    function Cart($cookies) {

        let cart = $cookies.getObject("cart") || {
            items: [],
            total: 0
        }

        console.log(cart);

        const updateCookie = function () {
            $cookies.putObject("cart", cart);
        }

        const getCartQuantitiesUpdated = function (item) {
            return cart.items
                .map(listItem => {
                    let check = listItem.id == item.id;
                    return { ...listItem, quantity: check ? listItem.quantity + 1 : 1 };
                });
        }

        const addNewItem = function (item) {
            let alreadyIn = cart.items
                .some(listItem => listItem.id == item.id);
            if (!alreadyIn) cart.items.push({ ...item, quantity: 1 });
        }

        const removeItem = function (item) {
            let [index] = cart.items
                .map((listItem, i) => listItem.id == item.id ? i : -1)
                .filter(item => item > -1);
            cart.items.splice(index, 1);
            updateCookie();
        }

        const getItems = () => cart.items;

        const getCartTotal = () => cart.total;

        const getCartQuantity = () => cart.items
            .map(item => item.quantity)
            .reduce((a, b) => a + b, 0);

        const add = (item) => {
            cart.items = getCartQuantitiesUpdated(item);
            addNewItem(item);
            updateCookie();
        }

        const remove = (item) => {
            removeItem(item);
        }

        const clean = () => {
            $cookies.remove('cart');
            cart = {
                items: [],
                total: 0
            }
        }

        return {
            add,
            remove,
            update: updateCookie,
            getItems,
            clean,
            getCartTotal,
            getCartQuantity
        }
    }

}());