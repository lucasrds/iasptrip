var express = require('express')
var serveStatic = require('serve-static')

var app = express()
console.log(__dirname);


app.use('/static', express.static(__dirname))
app.use('/css', express.static(__dirname + "/css"))
app.use('/view', express.static(__dirname + "/view"))
app.use('/controller', express.static(__dirname + "/controller"))
app.use('/directives', express.static(__dirname + "/directives"))

app.all('/*', function (req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile('index.html', { root: __dirname });
});


app.listen(8000)