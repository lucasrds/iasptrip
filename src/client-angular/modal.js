(function () {
    'use strict';

    angular
        .module('app')
        .service('$modal', ModalService)

    /** @ngInject */
    function ModalService($uibModal, $cookies, $api) {

        const info = function (message, title = "Info") {
            return $uibModal.open({
                templateUrl: '/view/info-modal.html',
                controllerAs: "$modal",
                controller: function ($uibModalInstance) {
                    let vm = this;
                    vm.message = message;
                    vm.title = title;
                    vm.close = $uibModalInstance.close;
                    vm.dismiss = $uibModalInstance.dismiss;
                }
            }).result;
        }

        const error = function (message, title = "Info") {
            return $uibModal.open({
                templateUrl: '/view/error-modal.html',
                controllerAs: "$modal",
                controller: function ($uibModalInstance) {
                    let vm = this;
                    vm.message = message;
                    vm.title = title;
                    vm.close = $uibModalInstance.close;
                    vm.dismiss = $uibModalInstance.dismiss;
                }
            }).result;
        }

        const flightDetail = function (flight) {
            return $uibModal.open({
                templateUrl: '/view/flight-detail-modal.html',
                controllerAs: "$modal",
                controller: function ($uibModalInstance) {
                    let vm = this;
                    vm.flight = flight;
                    vm.close = $uibModalInstance.close;
                    vm.dismiss = $uibModalInstance.dismiss;
                }
            }).result;
        }

        const loginModal = function () {
            return $uibModal.open({
                templateUrl: '/view/login-modal.html',
                controllerAs: "$modal",
                controller: function ($uibModalInstance) {
                    let vm = this;
                    vm.user = {
                        username: "",
                        password: ""
                    }

                    vm.userValidated = (user) => user.username && user.password;

                    vm.login = function (user) {
                        $api.user.login(user)
                            .then(response => {
                                $uibModalInstance.close(response.data)
                            })
                    }
                    vm.close = $uibModalInstance.close;
                    vm.dismiss = $uibModalInstance.dismiss;
                }
            }).result;
        }

        const registerModal = function () {
            return $uibModal.open({
                templateUrl: '/view/register-modal.html',
                controllerAs: "$modal",
                controller: function ($uibModalInstance) {
                    let vm = this;

                    vm.user = {
                        person: {},
                        username: "",
                        password: ""
                    }

                    vm.userValidated = function (user) {
                        return user.person.name && user.person.cpf
                            && user.username && user.password;
                    }

                    vm.register = function (user) {
                        $api.user.operations.save(user)
                            .then(response => $uibModalInstance.close(response.data))
                            .catch(err => {
                                if (err.data.message.includes("CPF")) info("Este CPF já é cadastrado", "Erro");
                                if (err.data.message.includes("USERNAME")) info("Este NOME DE USUÁRIO já é cadastrado", "Erro");
                            })
                    }
                    vm.close = $uibModalInstance.close;
                    vm.dismiss = $uibModalInstance.dismiss;
                }
            }).result;
        }

        const profileModal = function (id) {
            return $uibModal.open({
                templateUrl: '/view/profile-modal.html',
                controllerAs: "$modal",
                controller: function ($uibModalInstance) {
                    let vm = this;

                    vm.userValidated = (user) => user
                        && user.username
                        && user.password
                        && user.person.name
                        && user.person.cpf;

                    vm.update = function (user) {
                        $api.user.operations.save(user)
                            .then(response => $uibModalInstance.close(response.data))
                            .catch(err => {
                                if (err.data.message.includes("CPF")) info("Este CPF já é cadastrado", "Erro");
                                if (err.data.message.includes("USERNAME")) info("Este NOME DE USUÁRIO já é cadastrado", "Erro");
                            })
                    }


                    $api.user.operations.get(id)
                        .then(response => {
                            vm.user = response.data;
                        });
                    vm.close = $uibModalInstance.close;
                    vm.dismiss = $uibModalInstance.dismiss;
                }
            }).result;
        }

        const paymentModal = function (items, user) {
            return $uibModal.open({
                templateUrl: '/view/payment-modal.html',
                controllerAs: "$modal",
                resolve: {
                    config: $api => $api.config.list().then(r => r.data[0])
                },
                controller: function ($uibModalInstance, config) {
                    let vm = this;
                    vm.payment = {
                        cardNumber: "",
                        validity: "",
                        backCode: ""
                    }

                    vm.points = user.person.points || 0;

                    vm.totalWithPoints = function () {
                        let points = user.person.points;
                        let total = items
                            .map(item => item.value * item.quantity)
                            .reduce((a, b) => a + b, 0);
                        let discount = (points / 100) * total;
                        let valueToPay = total - discount;
                        return valueToPay;
                    }

                    function segmentFlightsList(items) {
                        let newItems = [];
                        items.forEach(flight => {
                            let flights = new Array(flight.quantity).fill({ ...flight, quantity: 1 });
                            flights.forEach(flight => newItems.push(flight))
                        });
                        return newItems;
                    }

                    function mapForDb(listItems) {
                        return listItems.map(flight => ({ flight }));
                    }

                    function totalToPay(items) {
                        return items
                            .map(flight => flight.value * flight.quantity)
                            .reduce((a, b) => a + b, 0);
                    }

                    function makePurchaseRequest(user, tickets, totalToPay) {
                        return {
                            user: { ...user, purchases: [] },
                            purchase: {
                                tickets,
                                value: totalToPay
                            }
                        };
                    }

                    function createPurchase(items, user) {
                        return new Promise(resolve =>
                            resolve(segmentFlightsList(items)))
                            .then(mapForDb)
                            .then(tickets =>
                                makePurchaseRequest(user, tickets, totalToPay(items)))
                    }

                    function createPurchaseWithPoints(items, user) {
                        return new Promise(resolve =>
                            resolve(segmentFlightsList(items)))
                            .then(mapForDb)
                            .then(tickets =>
                                makePurchaseRequest(user, tickets, vm.totalWithPoints()))
                    }

                    vm.paymentValidated = function (payment) {
                        return payment.cardNumber
                            && payment.validity
                            && payment.backCode;
                    }

                    vm.buyWithDiscount = function (params) {
                        createPurchaseWithPoints(items, user)
                            .then(purchaseRequest => $api.purchase
                                .purchaseWithPoints(purchaseRequest)
                                .then(response => {
                                    let cookieUser = $cookies.getObject("user");
                                    cookieUser.person.points = 0;
                                    $cookies.putObject("user", cookieUser);
                                    return response;
                                })
                                .then(response => {
                                    $uibModalInstance.close(response);
                                }));
                    }

                    vm.buy = function (payment) {
                        createPurchase(items, user)
                            .then(purchaseRequest => {
                                console.log(purchaseRequest);

                                $api.purchase.purchase(purchaseRequest)
                                    .then(response => {
                                        let cookieUser = $cookies.getObject("user");
                                        cookieUser.person.points = response.data;
                                        $cookies.putObject("user", cookieUser);
                                        return response;
                                    })
                                    .catch((err) => {
                                        $uibModalInstance.close(err);
                                    })
                                    .then(response => {
                                        $uibModalInstance.close(response);
                                    })

                            });
                    }

                    vm.close = $uibModalInstance.close;
                    vm.dismiss = $uibModalInstance.dismiss;
                }
            }).result;
        }

        return {
            info,
            error,
            flightDetail,
            loginModal,
            paymentModal,
            profileModal,
            registerModal
        }
    }

}());