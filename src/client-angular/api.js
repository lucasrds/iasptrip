(function () {
    'use strict';

    angular
        .module('app')
        .service('$api', Service)

    /** @ngInject */
    function Service($http) {
        let baseURL = 'http://localhost:8090/';
        let planeURL = baseURL + 'plane/';
        let locationURL = baseURL + 'location/';
        let flightURL = baseURL + 'flight/';
        let routeURL = baseURL + 'route/';
        let personURL = baseURL + 'person/';
        let purchaseURL = baseURL + 'purchase/';
        let userURL = baseURL + 'user/';
        let configURL = baseURL + 'config/';
        let airportURL = baseURL + 'airport/';

        let defaultAPI = (url) => ({
            list: () => $http.get(url),
            get: (id) => $http.get(url + id),
            save: (obj) => $http.post(url, obj),
            delete: (id) => $http.delete(url + id),
        });

        let userAPI = (url) => ({
            operations: defaultAPI(url),
            login: (user) => $http.post(url + 'login', user)
        });
        
        let purchaseAPI = Object.assign(defaultAPI(purchaseURL), {
            purchaseByClient: (client) => $http.post(purchaseURL + "purchasebyclient/", client),
            purchase: (obj) => $http.post(purchaseURL + "purchase/", obj),
            purchaseWithPoints: (obj) => $http.post(purchaseURL + "purchase/points", obj),
        })

        return {
            plane: defaultAPI(planeURL),
            location: defaultAPI(locationURL),
            flight: defaultAPI(flightURL),
            route: defaultAPI(routeURL),
            person: defaultAPI(personURL),
            airport: defaultAPI(airportURL),
            purchase: purchaseAPI,
            user: userAPI(userURL),
            config: defaultAPI(configURL),
        }
    }

}());