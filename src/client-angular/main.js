(function(){
    'use strict';

    angular
        .module('app', ['ngCookies', 'ui.router', 'ui.bootstrap', 'moment-picker']);

}());