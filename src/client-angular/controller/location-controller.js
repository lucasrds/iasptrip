(function () {
    'use strict';

    angular
        .module('app')
        .controller('LocationController', ControllerCtrl)

    let defaultLocation = {};
    /** @ngInject */
    function ControllerCtrl($api, $modal, $state, locations, location) {
        var vm = this;

        init();

        vm.delete = function (locationId) {
            $api.location.delete(locationId)
                .then(r => $state.reload(), err => {
                    if (err.data.message.includes('constraint')) {
                        $modal.info("Este avião está sendo utilizado em um voo");
                    }
                });
        }

        vm.submit = function () {
            $api.location.save(vm.location)
                .then(r => {
                    $state.go('^.locations')
                })
        }

        function init() {
            vm.locations = locations;
            console.log(locations);
            vm.location = location || defaultLocation;
        }

    }

}());