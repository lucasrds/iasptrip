(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($http, $api, $modal, $state, $cookies) {
        var vm = this;

        init();

        vm.login = function (user) {
            $api.user.login(user)
                .then(function (response) {
                    if(!response.data){
                        $modal.info("Usuário invalido", "Alerta");
                        return;
                    }
                    
                    $cookies.putObject("user", response.data);
                    $state.go('admin.clients', { user });
            })
        }
        function init() {
        }
    }
}());