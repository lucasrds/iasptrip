(function () {
    'use strict';

    angular
        .module('app')
        .controller('PurchaseController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($http, $api, $modal, $state, purchases, purchase) {
        var vm = this;

        init();

        vm.delete = function (purchaseId) {
            $api.purchase.delete(purchaseId)
                .then(r => $state.reload(), err => {
                    if (err.data.message.includes('constraint')) {
                        $modal.info("Esta rota está sendo utilizada em um voo");
                    }
                });
        }

        vm.submit = function () {
            $api.purchase.save(vm.purchase)
                .then(r => {
                    $state.go('^.purchases', { reload: true })
                })
        }

        function init() {
            vm.purchases = purchases.map(purchase => ({ ...purchase, date: new Date(purchase.date) }));
            vm.purchase = { ...purchase, date: new Date(purchase.date) };
        }

    }

}());