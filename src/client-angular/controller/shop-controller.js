(function () {
    'use strict';

    angular
        .module('app')
        .controller('ShopController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($http, $api, $modal, $state, $cart, flights) {
        var vm = this;

        init();

        function init() {

            vm.flights = flights.map(flight => ({
                ...flight,
                outDate: new Date(flight.outDate),
                arriveDate: new Date(flight.arriveDate)
            }));

            vm.details = function (flight) {
                $modal.flightDetail(flight);
            }

            vm.search = function (criteria) {
                return {
                    description: criteria
                }

            }

            vm.addToCart = function (flight) {
                $cart.add({
                    id: flight.id,
                    identifier: flight.identifier,
                    description: flight.description,
                    value: flight.value,
                    points: flight.points
                });
                $modal.info("Item adicionado ao carrinho!", "Sucesso");
            }

        }

    }

}());