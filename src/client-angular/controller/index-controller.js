(function () {
    'use strict';

    angular
        .module('app')
        .controller('IndexController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($modal, $timeout, $cookies, $state, $cart) {
        var vm = this;

        init();

        vm.loginDialog = function () {
            let modal = $modal.loginModal();
            modal.then(user => {
                if (!user) {
                    $modal.info("Usuário e/ou Senha inválido(s)", "Alerta");
                    return;
                }
                $cookies.putObject("user", user);
                $modal.info("Logado com sucesso!", "Sucesso")
                    .then($state.reload, () => {
                        $state.reload();
                    });
            })

        }

        vm.registerDialog = function () {
            let modal = $modal.registerModal();
            modal.then(response => {
                $modal.info("Cadastrado com sucesso!", "Sucesso");
                $state.reload();
            })
        }

        vm.profile = function (id) {
            $modal.profileModal(id)
                .then(response => {
                    $modal.info("Cadastrado com sucesso!", "Sucesso");
                    $state.reload();
                })
        }

        vm.logout = function () {
            $cookies.remove("user");
            $state.reload();
        }

        function init() {
            vm.loggedUser = $cookies.getObject("user");
            console.log(vm.loggedUser);

            vm.menuItems = [
                {
                    icon: "fas fa-home",
                    link: "/index",
                    text: "Home"
                },
                {
                    icon: "fas fa-plane",
                    link: "/index/voos",
                    text: "Vôos"
                }
            ]

            vm.cart = $cart;


        }

    }

}());