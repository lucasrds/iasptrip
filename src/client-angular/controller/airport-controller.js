(function () {
    'use strict';

    angular
        .module('app')
        .controller('AirportController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($http, $api, $modal, $state, airports, airport) {
        var vm = this;

        init();

        vm.delete = function (airportId) {
            $api.airport.delete(airportId)
                .then(r => $state.reload(), err => {
                    if (err.data.message.includes('constraint')) {
                        $modal.info("Este aeroporto está sendo utilizado e não pode ser deletado.");
                    }
                });
        }

        vm.validAirport = (airport) =>  airport.name 
            && airport.location.street
            && airport.location.state
            && airport.location.name
            && airport.location.zip
            && airport.location.city;

        vm.submit = function () {
            $api.airport.save(vm.airport)
                .then(r => {
                    $state.go('^.airports', { reload: true })
                })
        }


        let mapCepToZip = function (response) {
            return {
                street: response.logradouro,
                city: response.localidade,
                state: response.uf,
            }
        }

        let mergeLocation = function (stopover, address) {
            stopover.street = address.street;
            stopover.city = address.city;
            stopover.state = address.state;
        }

        vm.zipLookup = function (zip, index) {
            $http.get(`https://viacep.com.br/ws/${zip}/json/`)
                .then(response => {
                    let stopover = vm.airport.location;
                    mergeLocation(stopover, mapCepToZip(response.data));
                })
        }

        function init() {
            vm.airports = airports;
            vm.airport = {
                ...airport,
                outDate: new Date(airport.outDate),
                arriveDate: new Date(airport.arriveDate)
            };
        }

    }

}());