(function () {
    'use strict';

    angular
        .module('app')
        .controller('PlaneController', ControllerCtrl)

    let defaultPlane = {
        name: "",
        brand: "",
        seats: []
    };
    /** @ngInject */
    function ControllerCtrl($api, $modal, $state, planes, plane) {
        var vm = this;

        init();

        vm.selectBrand = function (brand) {
            vm.plane.brand = brand.name;
            vm.plane.seats = brand.seats;
        }

        vm.delete = function (planeId) {
            $api.plane.delete(planeId)
                .then(r => $state.reload(), err => {
                    if (err.data.message.includes('constraint')) {
                        $modal.info("Este avião está sendo utilizado em um voo");
                    }
                });
        }

        vm.validPlane = (plane) => plane.brand 
            && plane.seats 
            && plane.name; 

        vm.submit = function () {
            $api.plane.save(vm.plane)
                .then(r =>{
                    $state.go('^.planes')
                })
        }

        function init() {
            vm.planes = planes;
            vm.plane = plane || defaultPlane;

            vm.brands = [
                { name: "GOL", seats: 10 },
                { name: "TAM", seats: 5 }
            ]

            vm.selectedBrand = vm.brands
                .filter(b => b.name == plane.brand)[0];

        }

    }

}());