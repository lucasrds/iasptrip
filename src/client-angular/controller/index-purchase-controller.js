(function () {
    'use strict';

    angular
        .module('app')
        .controller('IndexPurchaseController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($http, $api, $modal, $state, person, purchase) {
        var vm = this;

        init();

        function init() {
            vm.purchases = person.purchases;
            vm.purchase = purchase;
        }

    }

}());