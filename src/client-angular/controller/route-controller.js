(function () {
    'use strict';

    angular
        .module('app')
        .controller('RouteController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($http, $api, $modal, $state, routes, route, airports) {
        var vm = this;

        init();

        let mapCepToZip = function (response) {
            return {
                street: response.logradouro,
                city: response.localidade,
                state: response.uf,
            }
        }

        let mergeLocation = function (stopover, address) {
            stopover.street = address.street;
            stopover.city = address.city;
            stopover.state = address.state;
        }

        vm.zipLookup = function (zip, index) {
            $http.get(`https://viacep.com.br/ws/${zip}/json/`)
                .then(response => {
                    let stopover = vm.route.stopovers[index];
                    mergeLocation(stopover, mapCepToZip(response.data));
                })
        }

        vm.delete = function (routeId) {
            $api.route.delete(routeId)
                .then(r => $state.reload(), err => {
                    if (err.data.message.includes('constraint')) {
                        $modal.info("Esta rota está sendo utilizada em um voo");
                    }
                });
        }

        vm.validRoute = function(route){
            return route.description && route.stopovers.length > 0;
        } 

        vm.submit = function () {
            vm.route.stopovers = vm.route.stopovers.map(stopover => ({
                ...stopover.location
            }));
            $api.route.save(vm.route)
                .then(r => {
                    $state.go('^.routes', { reload: true })
                })
        }

        function init() {
            vm.routes = routes;
            vm.route = route; 
            route.stopovers.forEach(stopover => {
                stopover.location = stopover;
            });
            vm.airports = airports;
        }

    }

}());