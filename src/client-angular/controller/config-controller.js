(function () {
    'use strict';

    angular
        .module('app')
        .controller('ConfigController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($http, $api, $modal, $state, $cart, config) {
        var vm = this;

        init();

        vm.submit = function () {
            $api.config.save(vm.config)
                .then(r => {
                    $modal.info("Salvo com sucesso!", "Sucesso");
                    $state.go('admin.config', { reload: true })
                })
        }
        function init() {
            vm.config = config;
        }

    }

}());