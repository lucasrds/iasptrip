(function () {
    'use strict';

    angular
        .module('app')
        .controller('FlightController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($http, $api, $modal, $state, flights, flight, planes, routes) {
        var vm = this;

        init();

        vm.delete = function (flightId) {
            $api.flight.delete(flightId)
                .then(r => $state.reload(), err => {
                    if (err.data.message.includes('constraint')) {
                        $modal.info("Este voo está sendo utilizado e não pode ser deletado");
                    }
                });
        }

        vm.validFlight = (flight) => flight.identifier
            && flight.description
            && flight.imagePath
            && flight.value
            && flight.points
            && flight.outDate
            && flight.arriveDate
            && flight.plane
            && flight.route;


        vm.submit = function () {
            $api.flight.save(vm.flight)
                .then(r => {
                    $state.go('^.flights', { reload: true })
                })
        }

        function init() {
            vm.flights = flights;
            vm.flight = {
                ...flight,
                outDate: flight.outDate ? new Date(flight.outDate) : new Date(),
                arriveDate: flight.arriveDate ? new Date(flight.arriveDate) : new Date()
            };
            vm.planes = planes;
            vm.routes = routes;
        }

    }

}());