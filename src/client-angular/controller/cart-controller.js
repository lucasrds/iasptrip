(function () {
    'use strict';

    angular
        .module('app')
        .controller('CartController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($api, $modal, $state, $cart, $cookies, items) {
        var vm = this;

        init();

        
        vm.totalValue = function () {
            return items
            .map(item => item.value * item.quantity)
            .reduce((a, b) => a + b, 0);
        }
        

        vm.removeItem = function (item) {
            $cart.remove(item);
        }

        vm.updateQuantity = function (item) {
            $cart.update();
        }

        vm.proceedPurchase = function () {
            if (!$cookies.getObject('user')) {
                $modal.info("Você precisa estar logado para comprar!", "Alerta");
                return;
            }
            let modal = $modal.paymentModal(vm.items, $cookies.getObject('user'));
            modal.then(response => {
                if (response.status == 403) {
                    $modal.error(response.data.message, "ERRO");
                }

                if (response.status == 200) {
                    $cart.clean();
                    $modal.info("Compra realizada com sucesso", "Sucesso")
                        .then(() => {
                            $state.go("index.shop", {reload:true});
                        }, () => {
                            $state.go("index.shop", {reload:true});
                        });

                }
            })
        }

        function init() {
            vm.items = items;
        }

    }

}());