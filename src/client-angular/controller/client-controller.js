(function () {
    'use strict';

    angular
        .module('app')
        .controller('ClientController', ControllerCtrl)

    /** @ngInject */
    function ControllerCtrl($http, $api, $modal, $state, clients, client) {
        var vm = this;

        init();

        vm.delete = function (clientId) {
            $api.client.delete(clientId)
                .then(r => $state.reload(), err => {
                    if (err.data.message.includes('constraint')) {
                        $modal.info("Esta rota está sendo utilizada em um voo");
                    }
                });
        }

        vm.submit = function () {
            $api.client.save(vm.client).then(r => {
                $state.go('^.clients', { reload: true })
            })
        }

        function init() {
            vm.clients = clients;
            vm.client = client;
            
            vm.client.purchases = client.purchases.map(purchase => ({
                ...purchase, date: new Date(purchase.date)
            }));;
        }

    }

}());