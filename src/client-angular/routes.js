(function () {
    'use strict';

    angular
        .module('app')
        .config(Config)

    /** @ngInject */
    function Config($locationProvider, $urlServiceProvider, $stateProvider) {
        $locationProvider.html5Mode(true);
        $urlServiceProvider.rules.otherwise({ state: 'index' });

        $stateProvider.state('index', {
            url: '/index',
            templateUrl: 'view/front/index.html',
            controller: "IndexController",
            controllerAs: "vm"
        });

        $stateProvider.state('index.shop', {
            url: '/voos',
            templateUrl: 'view/front/shop.html',
            controller: "ShopController",
            controllerAs: "vm",
            resolve: {
                flights: $api => $api.flight.list().then(r => r.data),
            }
        });

        $stateProvider.state('index.purchases', {
            url: '/pedidos/:id',
            templateUrl: 'view/front/purchases.html',
            controller: "IndexPurchaseController",
            controllerAs: "vm",
            resolve: {
                person: ($stateParams, $api) => $stateParams.id ? $api.person
                    .get($stateParams.id)
                    .then(r => r.data) : {},
                purchase: () => { }
            }
        });

        $stateProvider.state('index.purchase', {
            url: '/pedido/:id',
            templateUrl: 'view/purchase/purchase.html',
            controller: "IndexPurchaseController",
            controllerAs: "vm",
            resolve: {
                person: () => ({ purchases: [] }),
                purchase: ($stateParams, $api) => $stateParams.id ? $api.purchase
                    .get($stateParams.id)
                    .then(r => r.data) : {},
            }
        });

        $stateProvider.state('index.cart', {
            url: '/carrinho',
            templateUrl: 'view/front/cart.html',
            controller: "CartController",
            controllerAs: "vm",
            resolve: {
                items: $cart => $cart.getItems(),
            }
        });

        $stateProvider.state('adminLogin', {
            url: '/admin/login',
            templateUrl: 'view/login.html',
            controller: "LoginController",
            controllerAs: "loginCtrl"
        });

        $stateProvider.state('admin', {
            url: '/admin',
            templateUrl: 'view/admin.html',
            controller: "AdminController",
            params: { user: {} },
            resolve: { user: $stateParams => $stateParams.user }
        });

        $stateProvider.state('admin.planes', {
            url: '/avioes',
            templateUrl: 'view/plane/planes.html',
            controller: "PlaneController",
            controllerAs: "vm",
            resolve: {
                planes: $api => $api.plane.list().then(r => r.data),
                plane: () => ({})
            }
        });

        $stateProvider.state('admin.plane', {
            url: '/aviao/:id',
            templateUrl: 'view/plane/plane.html',
            controller: "PlaneController",
            controllerAs: "vm",
            resolve: {
                planes: () => [],
                plane: ($stateParams, $api) => $stateParams.id ? $api.plane
                    .get($stateParams.id)
                    .then(r => r.data) : {}
            }
        });

        $stateProvider.state('admin.config', {
            url: '/config',
            templateUrl: 'view/config.html',
            controller: "ConfigController",
            controllerAs: "vm",
            resolve: {
                config: $api => $api.config.list().then(r => r.data[0])
            }
        });

        $stateProvider.state('admin.locations', {
            url: '/locais',
            templateUrl: 'view/location/locations.html',
            controller: "LocationController",
            controllerAs: "vm",
            resolve: {
                locations: $api => $api.location.list().then(r => r.data),
                location: () => ({})
            }
        });

        $stateProvider.state('admin.location', {
            url: '/local/:id',
            templateUrl: 'view/location/location.html',
            controller: "LocationController",
            controllerAs: "vm",
            resolve: {
                locations: () => [],
                location: ($stateParams, $api) => $stateParams.id ?
                    $api.location.get($stateParams.id).then(r => r.data) : {}
            }
        });

        let defaultRouteModel = () => ({
            description: "",
            stopovers: []
        })

        $stateProvider.state('admin.routes', {
            url: '/rotas',
            templateUrl: 'view/route/routes.html',
            controller: "RouteController",
            controllerAs: "vm",
            resolve: {
                routes: $api => $api.route.list().then(r => r.data),
                route: defaultRouteModel,
                airports: () => []
            }
        });

        $stateProvider.state('admin.route', {
            url: '/rota/:id',
            templateUrl: 'view/route/route.html',
            controller: "RouteController",
            controllerAs: "vm",
            resolve: {
                routes: () => [],
                route: ($stateParams, $api) => $stateParams.id ? $api.route
                    .get($stateParams.id)
                    .then(r => r.data) : defaultRouteModel(),
                airports: $api => $api.airport.list().then(r => r.data)
            }
        });


        let defaultFlightModel = () => ({
            description: "",
            stopovers: []
        })

        $stateProvider.state('admin.flights', {
            url: '/voos',
            templateUrl: 'view/flight/flights.html',
            controller: "FlightController",
            controllerAs: "vm",
            resolve: {
                flights: $api => $api.flight.list().then(r => r.data),
                flight: defaultFlightModel,
                planes: () => [],
                routes: () => []
            }
        });

        $stateProvider.state('admin.flight', {
            url: '/voo/:id',
            templateUrl: 'view/flight/flight.html',
            controller: "FlightController",
            controllerAs: "vm",
            resolve: {
                flights: () => [],
                flight: ($stateParams, $api) => $stateParams.id ? $api.flight
                    .get($stateParams.id)
                    .then(r => r.data) : defaultFlightModel(),
                planes: ($api) => $api.plane.list().then(r => r.data),
                routes: ($api) => $api.route.list().then(r => r.data)
            }
        });


        let defaultAirportModel = () => ({
            name: "",
            location: {}
        })

        $stateProvider.state('admin.airports', {
            url: '/aeroportos',
            templateUrl: 'view/airport/airports.html',
            controller: "AirportController",
            controllerAs: "vm",
            resolve: {
                airports: $api => $api.airport.list().then(r => r.data),
                airport: defaultAirportModel
            }
        });

        $stateProvider.state('admin.airport', {
            url: '/aeroporto/:id',
            templateUrl: 'view/airport/airport.html',
            controller: "AirportController",
            controllerAs: "vm",
            resolve: {
                airports: () => [],
                airport: ($stateParams, $api) => $stateParams.id ? $api.airport
                    .get($stateParams.id)
                    .then(r => r.data) : defaultAirportModel()
            }
        });


        let defaultClientModel = () => ({ purchases: [] })

        $stateProvider.state('admin.clients', {
            url: '/clientes',
            templateUrl: 'view/client/clients.html',
            controller: "ClientController",
            controllerAs: "vm",
            resolve: {
                clients: $api => $api.person.list().then(r => r.data),
                client: defaultClientModel,
            }
        });

        $stateProvider.state('admin.client', {
            url: '/cliente/:id',
            templateUrl: 'view/client/client.html',
            controller: "ClientController",
            controllerAs: "vm",
            resolve: {
                clients: () => [],
                client: ($stateParams, $api) => $stateParams.id ? $api.person
                    .get($stateParams.id)
                    .then(r => r.data) : defaultClientModel(),
            }
        });


        let defaultPurchaseModel = () => ({})

        $stateProvider.state('admin.purchases', {
            url: '/vendas',
            templateUrl: 'view/purchase/purchases.html',
            controller: "PurchaseController",
            controllerAs: "vm",
            resolve: {
                purchases: $api => $api.purchase.list().then(r => r.data),
                purchase: defaultPurchaseModel,
            }
        });

        $stateProvider.state('admin.purchase', {
            url: '/venda/:id',
            templateUrl: 'view/purchase/purchase.html',
            controller: "PurchaseController",
            controllerAs: "vm",
            resolve: {
                purchases: () => [],
                purchase: ($stateParams, $api) => $stateParams.id ? $api.purchase
                    .get($stateParams.id)
                    .then(r => r.data) : defaultPurchaseModel(),
            }
        });

    }

}());