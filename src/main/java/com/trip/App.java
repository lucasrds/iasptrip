package com.trip;

import com.trip.application.entity.*;
import com.trip.application.repository.address.AddressRepository;
import com.trip.application.repository.airport.AirportRepository;
import com.trip.application.repository.config.ConfigRepository;
import com.trip.application.repository.flight.FlightRepository;
import com.trip.application.repository.location.LocationRepository;
import com.trip.application.repository.person.PersonRepository;
import com.trip.application.repository.plane.PlaneRepository;
import com.trip.application.repository.purchase.PurchaseRepository;
import com.trip.application.repository.route.RouteRepository;
import com.trip.application.repository.ticket.TicketRepository;
import com.trip.application.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
@EnableJpaRepositories
public class App implements CommandLineRunner {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private AirportRepository airportRepository;

    @Autowired
    private PlaneRepository planeRepository;

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private ConfigRepository configRepository;

    public static void main(String[] args) {

        SpringApplication.run(App.class);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        // 1. Separar camada de dominio da camada hibernate.
        // 2. Contexto dos repositorios esta diferente. -> Arrumado com @Transactional

        //Setup some default data

        Person admin = new Person("Admin", "999");
        admin.setLevel(1);
        admin.setPoints(50);

        User adminUser = new User();
        adminUser.setPerson(admin);
        adminUser.setUsername("admin");
        adminUser.setPassword("123");


        Person person = new Person("Lucas", "123");
        person.setLevel(1);
        person.setPoints(50);

        User user = new User();
        user.setPerson(person);
        user.setUsername("123");
        user.setPassword("123");
        
        userRepository.save(user);
        userRepository.save(adminUser);

        Plane plane = new Plane();
        plane.setName("Avião A01");
        plane.setBrand("GOL");
        plane.setSeats(10);
//        for (int i : new int[20]) {
//            Seat seat = new Seat(i, false);
//            plane.addSeat(seat);
//        }
        
        Plane plane2 = new Plane();
        plane2.setName("Avião A02");
        plane2.setBrand("TAM");
        plane2.setSeats(20);
//        for (int i : new int[20]) {
//            Seat seat = new Seat(i, false);
//            plane2.addSeat(seat);
//        }

        planeRepository.save(plane);
        planeRepository.save(plane2);

        Location locationSP = new Location();
        locationSP.setName("SP");
        locationSP.setCity("SP");
        locationSP.setState("SP");
        locationSP.setStreet("SP101");
        locationSP.setZip("1889912");
        locationSP.setLatitude(-46633309L);
        locationSP.setLongitude(-23550520L);
        Airport airportA = new Airport();
        airportA.setLocation(locationSP);
        airportA.setName("Guarulhos");

        airportA = airportRepository.save(airportA);

        Location locationEUA = new Location();
        locationEUA.setName("New York");
        locationEUA.setCity("EUA");
        locationEUA.setState("EUA");
        locationEUA.setStreet("EUA101");
        locationEUA.setZip("1889912");
        locationEUA.setLatitude(40689249L);
        locationEUA.setLongitude(-74044500L);
        Airport airportB = new Airport();
        airportB.setLocation(locationEUA);
        airportB.setName("Albany");

        airportB = airportRepository.save(airportB);

        List<Airport> stopOvers = new ArrayList<>();
        stopOvers.add(airportA);
        stopOvers.add(airportB);

        Location india = new Location();
        india.setName("India");
        india.setCity("India");
        india.setState("India");
        india.setStreet("India");
        india.setZip("1889912");
        india.setLatitude(40689249L);
        india.setLongitude(-74044500L);
        Airport airportC = new Airport();
        airportC.setLocation(india);
        airportC.setName("Indiaport");

        airportC = airportRepository.save(airportC);

        Route fromBrazilToAmerica = new Route(stopOvers);
        fromBrazilToAmerica.setDescription("From Brazil to New York");
        routeRepository.save(fromBrazilToAmerica);

        Flight brazilAmericaFlight = new Flight(plane, fromBrazilToAmerica);
        brazilAmericaFlight.setDescription("Flight 01");
        brazilAmericaFlight.setIdentifier("F01");
        brazilAmericaFlight.setArriveDate(new Date());
        brazilAmericaFlight.setOutDate(new Date());
        brazilAmericaFlight.setValue(555L);
        brazilAmericaFlight.setAvaiableSeats(plane.getSeats());
        brazilAmericaFlight.setPoints(50);
        brazilAmericaFlight.setImagePath("https://novayork.com/sites/default/files/styles/md_slider_1_bg/public/nyc-downtown-statue-of-liberty-750.jpg?itok=FfbAk70n");
        flightRepository.save(brazilAmericaFlight);

        List<Airport> stopOvers2 = new ArrayList<>();
        stopOvers2.add(airportC);

        Route fromBrazilToIndia = new Route(stopOvers2);
        fromBrazilToIndia.setDescription("From Brazil to India");
        routeRepository.save(fromBrazilToIndia);

        Flight brazilIndia = new Flight(plane, fromBrazilToIndia);
        brazilIndia.setDescription("Voo do Brasil até a India");
        brazilIndia.setIdentifier("F02");
        brazilIndia.setArriveDate(new Date());
        brazilIndia.setOutDate(new Date());
        brazilIndia.setValue(1700L);
        brazilIndia.setAvaiableSeats(plane.getSeats());
        brazilIndia.setPoints(20);
        brazilIndia.setImagePath("https://accuratebackground.com/wp-content/uploads/2014/04/indiabanner_mob.jpg");
        flightRepository.save(brazilIndia);

        Config config = new Config();
        config.setId(1);
        config.setLevelOnePercent(0.1);
        config.setLevelTwoPercent(0.25);
        config.setLevelThreePercent(1);
        //CADA PONTO VAI VALER 1 PORCENTO DE DESCONTO
        configRepository.save(config);



//        Ticket ticketBrazilAmerica = new Ticket();
//        ticketBrazilAmerica.setFlight(brazilAmericaFlight);
////        ticketBrazilAmerica.setSeat(plane.getSeats().get(0));
//        ticketRepository.save(ticketBrazilAmerica);
//
//        Purchase purchase = new Purchase();
//        purchase.setValue(555L);
//        purchase.addTicket(ticketBrazilAmerica);
//
//        purchaseRepository.save(purchase);
//
//        person.addPurchase(purchase);

    }

}
