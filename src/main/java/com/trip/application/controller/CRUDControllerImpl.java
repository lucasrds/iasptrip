package com.trip.application.controller;

import com.trip.application.service.ServiceBehavior;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import java.util.HashMap;
import java.util.Map;

public abstract class CRUDControllerImpl<T, ID extends Serializable> implements CRUDController<T, ID> {

    protected ServiceBehavior<T, ID> service;

    CRUDControllerImpl(ServiceBehavior<T, ID> service) {

        this.service = service;
    }

    @PostMapping(produces = "application/json")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<?> insert(@RequestBody T object) {

        service.insert(object);
        Map<String,String> response = new HashMap<>();
        response.put("response", "true");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<T>> list() {

        List<T> allT = service.findAll();
        if (allT.isEmpty()) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
        return new ResponseEntity<>(allT, HttpStatus.OK);
    }

    @GetMapping(value = "/{objectId}", produces = "application/json")
    public ResponseEntity<T> get(@PathVariable ID objectId) {

        Optional<T> optional = this.service.findOne(objectId);
        return optional.map(t -> new ResponseEntity<>(t, HttpStatus.OK))
                       .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{objectId}")
    public ResponseEntity<?> delete(@PathVariable ID objectId) {

        service.delete(objectId);
        return ResponseEntity.noContent()
                             .build();
    }

    @PutMapping("/{objectId}")
    public ResponseEntity<?> update(@PathVariable ID objectId, @RequestBody T object) {

        service.update(object);
        return new ResponseEntity<>("object Updated", HttpStatus.OK);
    }

}
