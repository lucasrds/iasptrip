package com.trip.application.controller;

import com.trip.application.service.seat.SeatService;
import com.trip.application.entity.Seat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/seat")
public class SeatRestController extends CRUDControllerImpl<Seat, Integer> {

    private SeatService seatService;

    @Autowired
    public SeatRestController(SeatService seatService) {

        super(seatService);
    }
}
