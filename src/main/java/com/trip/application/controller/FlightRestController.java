package com.trip.application.controller;

import com.trip.application.service.flight.FlightService;
import com.trip.application.entity.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/flight")
public class FlightRestController extends CRUDControllerImpl<Flight, Integer> {

    private FlightService flightService;

    @Autowired
    public FlightRestController(FlightService flightService) {

        super(flightService);
    }
}
