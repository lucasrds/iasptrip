package com.trip.application.controller;

import com.trip.application.entity.User;
import com.trip.application.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserRestController extends CRUDControllerImpl<User, Integer> {

    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {

        super(userService);
        this.userService = userService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody User user) {
        
        return new ResponseEntity<>(userService.login(user), HttpStatus.OK);
    }
}
