package com.trip.application.controller;

import com.trip.application.service.location.LocationService;
import com.trip.application.entity.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/location")
public class LocationRestController extends CRUDControllerImpl<Location, Integer> {

    private LocationService locationService;

    @Autowired
    public LocationRestController(LocationService locationService) {

        super(locationService);
    }
}
