package com.trip.application.controller;

import com.trip.application.service.address.AddressService;
import com.trip.application.entity.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/address")
public class AddressRestController extends CRUDControllerImpl<Address, Integer> {

    private AddressService addressService;

    @Autowired
    public AddressRestController(AddressService addressService) {

        super(addressService);
    }
}
