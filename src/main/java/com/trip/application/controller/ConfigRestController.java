package com.trip.application.controller;

import com.trip.application.entity.Config;
import com.trip.application.service.config.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
public class ConfigRestController extends CRUDControllerImpl<Config, Integer> {

    private ConfigService  configService;

    @Autowired
    public ConfigRestController(ConfigService configService) {

        super(configService);
        this.configService = configService;
    }

}
