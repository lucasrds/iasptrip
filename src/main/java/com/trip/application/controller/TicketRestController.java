package com.trip.application.controller;

import com.trip.application.service.ticket.TicketService;
import com.trip.application.entity.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ticket")
public class TicketRestController extends CRUDControllerImpl<Ticket, Integer> {

    private TicketService ticketService;

    @Autowired
    public TicketRestController(TicketService ticketService) {

        super(ticketService);
    }
}
