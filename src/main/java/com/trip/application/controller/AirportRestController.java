package com.trip.application.controller;

import com.trip.application.entity.Airport;
import com.trip.application.service.airport.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/airport")
public class AirportRestController extends CRUDControllerImpl<Airport, Integer> {

    private AirportService airportService;

    @Autowired
    public AirportRestController(AirportService airportService) {

        super(airportService);
        this.airportService = airportService;
    }
}
