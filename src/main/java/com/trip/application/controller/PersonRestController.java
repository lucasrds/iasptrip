package com.trip.application.controller;

import com.trip.application.service.person.PersonService;
import com.trip.application.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonRestController extends CRUDControllerImpl<Person, Integer> {

    private PersonService personService;

    @Autowired
    public PersonRestController(PersonService personService) {

        super(personService);
    }
}
