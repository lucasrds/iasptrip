package com.trip.application.controller;

import com.trip.application.service.route.RouteService;
import com.trip.application.entity.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/route")
public class RouteRestController extends CRUDControllerImpl<Route, Integer> {

    private RouteService routeService;

    @Autowired
    public RouteRestController(RouteService routeService) {

        super(routeService);
    }
}
