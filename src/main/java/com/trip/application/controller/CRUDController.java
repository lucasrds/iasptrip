package com.trip.application.controller;

import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CRUDController<T, ID> {

    ResponseEntity<?> insert(T object);

    ResponseEntity<List<T>> list();

    ResponseEntity<T> get(ID objectId);

    ResponseEntity<?> delete(ID objectId);
}
