package com.trip.application.controller;

import com.trip.application.entity.Purchase;
import com.trip.application.entity.PurchaseRequest;
import com.trip.application.service.purchase.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/purchase")
public class PurchaseRestController extends CRUDControllerImpl<Purchase, Integer> {

    private PurchaseService purchaseService;

    @Autowired
    public PurchaseRestController(PurchaseService purchaseService) {

        super(purchaseService);
        this.purchaseService = purchaseService;
    }

    @PostMapping(value = "/purchase")
    public ResponseEntity<?> purchase(@RequestBody PurchaseRequest requestBody) {

        return new ResponseEntity<>(purchaseService.purchase(requestBody), HttpStatus.OK);
    }

    @PostMapping(value = "/purchase/points")
    public ResponseEntity<?> purchasePoints(@RequestBody PurchaseRequest requestBody) {

        return new ResponseEntity<>(purchaseService.purchaseWithPoint(requestBody), HttpStatus.OK);
    }
}
