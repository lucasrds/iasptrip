package com.trip.application.controller;

import com.trip.application.service.plane.PlaneService;
import com.trip.application.entity.Plane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/plane")
public class PlaneRestController extends CRUDControllerImpl<Plane, Integer> {

    private PlaneService planeService;

    @Autowired
    public PlaneRestController(PlaneService planeService) {

        super(planeService);
    }
}
