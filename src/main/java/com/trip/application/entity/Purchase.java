package com.trip.application.entity;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "Purchase")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToMany(cascade = {CascadeType.ALL})
    @JsonIdentityReference(alwaysAsId = true)
    private List<Ticket> tickets = new ArrayList<>();

    private Date date = new Date();
    private long value;
    private UUID uuid = UUID.randomUUID();

    public Purchase() {

    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void addTickets(List<Ticket> tickets) {
        this.tickets.addAll(tickets);
    }

    public void addTicket(Ticket ticket) {
        this.tickets.add(ticket);
    }

    public void setTickets(List<Ticket> tickets) {

        this.tickets = tickets;
    }

    public Date getDate() {

        return date;
    }

    public void setDate(Date date) {

        this.date = date;
    }

    public long getValue() {

        return value;
    }

    public void setValue(long value) {

        this.value = value;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
