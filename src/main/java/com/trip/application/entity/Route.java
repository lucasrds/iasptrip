package com.trip.application.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Table(name = "Route")
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

//    @OneToMany(cascade = { CascadeType.ALL })
//    private List<Location> stopOvers;

    @ManyToMany
    private List<Airport> stopOvers;

    private String description;

    public Route() {

    }

    public Route(List<Airport> stopOvers) {

        this.stopOvers = stopOvers;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    @JsonProperty("stopovers")
    public List<Airport> getStopOvers() {

        return stopOvers;
    }

    public void setStopOvers(List<Airport> stopOvers) {

        this.stopOvers = stopOvers;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

}
