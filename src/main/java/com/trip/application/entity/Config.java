package com.trip.application.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Table(name = "Config")
public class Config {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private double levelOnePercent;
    private double levelTwoPercent;
    private double levelThreePercent;

    public Config() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLevelOnePercent() {
        return levelOnePercent;
    }

    public void setLevelOnePercent(double levelOnePercent) {
        this.levelOnePercent = levelOnePercent;
    }

    public double getLevelTwoPercent() {
        return levelTwoPercent;
    }

    public void setLevelTwoPercent(double levelTwoPercent) {
        this.levelTwoPercent = levelTwoPercent;
    }

    public double getLevelThreePercent() {
        return levelThreePercent;
    }

    public void setLevelThreePercent(double levelThreePercent) {
        this.levelThreePercent = levelThreePercent;
    }
}
