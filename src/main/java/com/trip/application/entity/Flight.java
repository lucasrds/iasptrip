package com.trip.application.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "Flight")
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    private Plane plane;

    @OneToOne
    private Route route;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Person> customers = new ArrayList<>();

    private String identifier;
    private String imagePath;
    private String description;
    private Date outDate;
    private Date arriveDate;
    private long value;
    private int avaiableSeats = 0;
    private int points = 0;

    public Flight() {

    }

    public Flight(Plane plane, Route route) {

        this.plane = plane;
        this.route = route;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    @JsonProperty("plane")
    public Plane getPlane() {

        return plane;
    }

    public void setPlane(Plane plane) {

        this.plane = plane;
    }

    @JsonProperty("route")
    public Route getRoute() {

        return route;
    }

    public void setRoute(Route route) {

        this.route = route;
    }

    public List<Person> getCustomers() {

        return customers;
    }

    public void setCustomers(List<Person> customers) {

        this.customers = customers;
    }

    public void addCustomer(Person customer) {

        this.customers.add(customer);
    }

    public String getIdentifier() {

        return identifier;
    }

    public void setIdentifier(String identifier) {

        this.identifier = identifier;
    }

    public String getImagePath() {

        return imagePath;
    }

    public void setImagePath(String imagePath) {

        this.imagePath = imagePath;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public Date getOutDate() {

        return outDate;
    }

    public void setOutDate(Date outDate) {

        this.outDate = outDate;
    }

    public Date getArriveDate() {

        return arriveDate;
    }

    public void setArriveDate(Date arriveDate) {

        this.arriveDate = arriveDate;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public int getAvaiableSeats() {
        return avaiableSeats;
    }

    public void setAvaiableSeats(int avaiableSeats) {
        this.avaiableSeats = avaiableSeats;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
