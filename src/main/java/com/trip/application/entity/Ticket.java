package com.trip.application.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.UUID;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler","tickets" })
@Table(name = "Ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    private Flight flight;

    private UUID uuid = UUID.randomUUID();

//    @OneToOne
//    private Seat seat;

    public Ticket() {

    }

    @JsonProperty("flight")
    public Flight getFlight() {

        return flight;
    }

    public void setFlight(Flight flight) {

        this.flight = flight;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
    //    @JsonProperty("seat")
//    public Seat getSeat() {
//
//        return seat;
//    }
//
//    public void setSeat(Seat seat) {
//
//        this.seat = seat;
//    }

}
