package com.trip.application.entity;

public class PurchaseRequest {
    User user;
    Purchase purchase;

    public PurchaseRequest() {
    }

    public PurchaseRequest(User user, Purchase purchase) {
        this.user = user;
        this.purchase = purchase;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }
}
