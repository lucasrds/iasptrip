package com.trip.application.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id")
@Table(name = "Person")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    private String name;

    @NotNull
    @Column(unique = true)
    private String cpf;

    @OneToMany
    // @JsonIgnoreProperties( "tickets" )
    private List<Purchase> purchases = new ArrayList<>();

    private int level;
    private double points;

    public Person() {

    }

    public Person(String name, String cpf) {

        this.name = name;
        this.cpf = cpf;
    }

    public Person(String name, String cpf, String apartmentNumber) {

        this.name = name;
        this.cpf = cpf;
    }

    public long getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getCpf() {

        return cpf;
    }

    public void setCpf(String cpf) {

        this.cpf = cpf;
    }

    @JsonProperty("purchases")
    public List<Purchase> getPurchases() {

        return purchases;
    }

    public void setPurchases(List<Purchase> purchases) {

        this.purchases = purchases;
    }

    public void addPurchase(Purchase purchase){
        this.purchases.add(purchase);
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }
    public  void addPoints(double points){
        this.points += points;
    }
}
