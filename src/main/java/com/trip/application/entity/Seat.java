package com.trip.application.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Table(name = "Seat")
public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int identifier;
    private boolean reserved;

    public Seat() {

    }

    public Seat(int identifier, boolean reserved) {

        this.identifier = identifier;
        this.reserved = reserved;
    }

    public long getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public boolean isReserved() {

        return reserved;
    }

    public void setReserved(boolean reserved) {

        this.reserved = reserved;
    }

    public int getIdentifier() {

        return identifier;
    }

    public void setIdentifier(int identifier) {

        this.identifier = identifier;
    }

}
