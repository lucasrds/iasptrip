package com.trip.application.repository.person;

import com.trip.application.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface PersonRepository extends JpaRepository<Person, Integer> {

    void delete(int id);

    boolean exists(int id);

}
