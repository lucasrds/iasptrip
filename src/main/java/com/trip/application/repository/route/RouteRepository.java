package com.trip.application.repository.route;

import com.trip.application.entity.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface RouteRepository extends JpaRepository<Route, Integer> {

    void delete(int id);

    boolean exists(int id);

}
