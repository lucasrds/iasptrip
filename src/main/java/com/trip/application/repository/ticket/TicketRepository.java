package com.trip.application.repository.ticket;

import com.trip.application.entity.Flight;
import com.trip.application.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TicketRepository extends JpaRepository<Ticket, Integer> {

    void delete(int id);

    boolean exists(int id);

    List<Ticket> findByFlight(Flight flight);

}
