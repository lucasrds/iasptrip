package com.trip.application.repository.purchase;

import com.trip.application.entity.Flight;
import com.trip.application.entity.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {

    void delete(int id);

    boolean exists(int id);

    List<Purchase> findByTicketsFlight(Flight flight);

}
