package com.trip.application.repository.plane;

import com.trip.application.entity.Plane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface PlaneRepository extends JpaRepository<Plane, Integer> {

    void delete(int id);

    boolean exists(int id);

}
