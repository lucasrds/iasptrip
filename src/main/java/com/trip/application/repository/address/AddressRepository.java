package com.trip.application.repository.address;

import com.trip.application.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface AddressRepository extends JpaRepository<Address, Integer> {

    void delete(int id);

    boolean exists(int id);

}
