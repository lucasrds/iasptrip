package com.trip.application.repository.location;

import com.trip.application.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface LocationRepository extends JpaRepository<Location, Integer> {

    void delete(int id);

    boolean exists(int id);

}
