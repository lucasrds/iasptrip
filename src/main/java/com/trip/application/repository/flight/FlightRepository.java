package com.trip.application.repository.flight;

import com.trip.application.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface FlightRepository extends JpaRepository<Flight, Integer> {

    void delete(int id);

    boolean exists(int id);

}
