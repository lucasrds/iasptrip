package com.trip.application.repository.airport;

import com.trip.application.entity.Airport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface AirportRepository extends JpaRepository<Airport, Integer> {

    void delete(int id);

    boolean exists(int id);

}
