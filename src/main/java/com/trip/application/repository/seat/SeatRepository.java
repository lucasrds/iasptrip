package com.trip.application.repository.seat;

import com.trip.application.entity.Seat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface SeatRepository extends JpaRepository<Seat, Integer> {

    void delete(int id);

    boolean exists(int id);

}
