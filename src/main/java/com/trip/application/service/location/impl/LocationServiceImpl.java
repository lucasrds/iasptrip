package com.trip.application.service.location.impl;

import com.trip.application.repository.location.LocationRepository;
import com.trip.application.service.location.LocationService;
import com.trip.application.entity.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LocationServiceImpl implements LocationService {

    private LocationRepository locationRepository;

    @Autowired
    public LocationServiceImpl(LocationRepository locationRepository) {

        this.locationRepository = locationRepository;
    }

    @Override
    public boolean insert(Location location) {

        locationRepository.save(location);
        return true;
    }

    @Override
    public boolean update(Location location) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        locationRepository.delete(id);
        return true;
    }

    @Override
    public List<Location> findAll() {

        return locationRepository.findAll();
    }

    @Override
    public Optional<Location> findOne(Integer id) {

        return Optional.ofNullable(locationRepository.findOne(id));
    }
}
