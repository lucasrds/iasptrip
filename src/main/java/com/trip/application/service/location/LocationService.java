package com.trip.application.service.location;

import com.trip.application.service.ServiceBehavior;
import com.trip.application.entity.Location;

public interface LocationService extends ServiceBehavior<Location, Integer> {

}
