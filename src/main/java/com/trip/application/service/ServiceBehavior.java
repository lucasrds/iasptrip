package com.trip.application.service;

public interface ServiceBehavior<T, ID> extends InputService<T, ID>, OutputService<T, ID> {

}
