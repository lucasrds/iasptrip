package com.trip.application.service.airport.impl;

import com.trip.application.entity.Airport;
import com.trip.application.repository.airport.AirportRepository;
import com.trip.application.service.airport.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AirportServiceImpl implements AirportService {

    private AirportRepository airportRepository;

    @Autowired
    public AirportServiceImpl(AirportRepository airportRepository) {

        this.airportRepository = airportRepository;
    }

    @Override
    public boolean insert(Airport airport) {

        airportRepository.save(airport);
        return true;
    }

    @Override
    public boolean update(Airport airport) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        airportRepository.delete(id);
        return true;
    }

    @Override
    public List<Airport> findAll() {

        return airportRepository.findAll();
    }

    @Override
    public Optional<Airport> findOne(Integer id) {

        return Optional.ofNullable(airportRepository.findOne(id));
    }
}
