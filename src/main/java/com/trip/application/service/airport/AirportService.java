package com.trip.application.service.airport;

import com.trip.application.entity.Airport;
import com.trip.application.service.ServiceBehavior;

public interface AirportService extends ServiceBehavior<Airport, Integer> {

}
