package com.trip.application.service.config.impl;

import com.trip.application.entity.Config;
import com.trip.application.repository.config.ConfigRepository;
import com.trip.application.service.config.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ConfigServiceImpl implements ConfigService {

    private ConfigRepository configRepository;

    @Autowired
    public ConfigServiceImpl(ConfigRepository configRepository) {

        this.configRepository = configRepository;
    }

    @Override
    public boolean insert(Config config) {

        configRepository.save(config);
        return true;
    }

    @Override
    public boolean update(Config config) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        configRepository.delete(id);
        return true;
    }

    @Override
    public List<Config> findAll() {

        return configRepository.findAll();
    }

    @Override
    public Optional<Config> findOne(Integer id) {

        return Optional.ofNullable(configRepository.findOne(id));
    }
}
