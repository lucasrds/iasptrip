package com.trip.application.service.config;

import com.trip.application.entity.Config;
import com.trip.application.entity.Flight;
import com.trip.application.service.ServiceBehavior;

public interface ConfigService extends ServiceBehavior<Config, Integer> {

}
