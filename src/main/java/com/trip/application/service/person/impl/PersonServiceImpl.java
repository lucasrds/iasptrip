package com.trip.application.service.person.impl;

import com.trip.application.repository.person.PersonRepository;
import com.trip.application.service.person.PersonService;
import com.trip.application.entity.Person;
import com.trip.application.entity.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {

    private PersonRepository personRepository;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository) {

        this.personRepository = personRepository;
    }

    @Override
    public boolean insert(Person person) {

        personRepository.save(person);
        return true;
    }

    @Override
    public boolean update(Person person) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        personRepository.delete(id);
        return true;
    }

    @Override
    public List<Person> findAll() {

        return personRepository.findAll();
    }

    @Override
    public Optional<Person> findOne(Integer id) {

        return Optional.ofNullable(personRepository.findOne(id));
    }

    @Override
    public void purchase(Purchase purchase) {
        //TODO: implement purchase
    }
}
