package com.trip.application.service.person;

import com.trip.application.service.ServiceBehavior;
import com.trip.application.entity.Person;
import com.trip.application.entity.Purchase;

public interface PersonService extends ServiceBehavior<Person, Integer> {
    void purchase(Purchase purchase);
}
