package com.trip.application.service;

public interface InputService<T, ID> {

    boolean insert(T t);

    boolean update(T t);

    boolean delete(ID id);

}
