package com.trip.application.service.seat.impl;

import com.trip.application.repository.seat.SeatRepository;
import com.trip.application.service.seat.SeatService;
import com.trip.application.entity.Seat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SeatServiceImpl implements SeatService {

    private SeatRepository seatRepository;

    @Autowired
    public SeatServiceImpl(SeatRepository seatRepository) {

        this.seatRepository = seatRepository;
    }

    @Override
    public boolean insert(Seat seat) {

        seatRepository.save(seat);
        return true;
    }

    @Override
    public boolean update(Seat seat) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        seatRepository.delete(id);
        return true;
    }

    @Override
    public List<Seat> findAll() {

        return seatRepository.findAll();
    }

    @Override
    public Optional<Seat> findOne(Integer id) {

        return Optional.ofNullable(seatRepository.findOne(id));
    }
}
