package com.trip.application.service.seat;

import com.trip.application.service.ServiceBehavior;
import com.trip.application.entity.Seat;

public interface SeatService extends ServiceBehavior<Seat, Integer> {

}
