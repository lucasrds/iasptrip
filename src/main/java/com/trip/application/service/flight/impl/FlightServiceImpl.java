package com.trip.application.service.flight.impl;

import com.trip.application.repository.flight.FlightRepository;
import com.trip.application.service.flight.FlightService;
import com.trip.application.entity.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class FlightServiceImpl implements FlightService {

    private FlightRepository flightRepository;

    @Autowired
    public FlightServiceImpl(FlightRepository flightRepository) {

        this.flightRepository = flightRepository;
    }

    @Override
    public boolean insert(Flight flight) {

        flightRepository.save(flight);
        return true;
    }

    @Override
    public boolean update(Flight flight) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        flightRepository.delete(id);
        return true;
    }

    @Override
    public List<Flight> findAll() {

        return flightRepository.findAll();
    }

    @Override
    public Optional<Flight> findOne(Integer id) {

        return Optional.ofNullable(flightRepository.findOne(id));
    }
}
