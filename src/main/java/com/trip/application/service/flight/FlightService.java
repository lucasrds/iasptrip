package com.trip.application.service.flight;

import com.trip.application.service.ServiceBehavior;
import com.trip.application.entity.Flight;

public interface FlightService extends ServiceBehavior<Flight, Integer> {

}
