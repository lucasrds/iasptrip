package com.trip.application.service.purchase;

import com.trip.application.controller.PurchaseRestController;
import com.trip.application.entity.Purchase;
import com.trip.application.entity.PurchaseRequest;
import com.trip.application.service.ServiceBehavior;

public interface PurchaseService extends ServiceBehavior<Purchase, Integer> {
    double purchase(PurchaseRequest request);
    double purchaseWithPoint(PurchaseRequest request);
}
