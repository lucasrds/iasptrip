package com.trip.application.service.purchase.impl;

import com.trip.application.entity.*;
import com.trip.application.exceptions.NoMoreTicketsException;
import com.trip.application.repository.config.ConfigRepository;
import com.trip.application.repository.flight.FlightRepository;
import com.trip.application.repository.purchase.PurchaseRepository;
import com.trip.application.repository.ticket.TicketRepository;
import com.trip.application.repository.user.UserRepository;
import com.trip.application.service.purchase.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PurchaseServiceImpl implements PurchaseService {

    private PurchaseRepository purchaseRepository;
    private UserRepository userRepository;
    private FlightRepository flightRepository;
    private ConfigRepository configRepository;
    private TicketRepository ticketRepository;

    @Autowired
    public PurchaseServiceImpl(PurchaseRepository purchaseRepository,
                               UserRepository userRepository,
                               FlightRepository flightRepository,
                               TicketRepository ticketRepository,
                               ConfigRepository configRepository
    ) {

        this.purchaseRepository = purchaseRepository;
        this.userRepository = userRepository;
        this.flightRepository = flightRepository;
        this.ticketRepository = ticketRepository;
        this.configRepository = configRepository;
    }

    @Override
    public boolean insert(Purchase purchase) {

        purchaseRepository.save(purchase);
        return true;
    }

    @Override
    public boolean update(Purchase purchase) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        purchaseRepository.delete(id);
        return true;
    }

    @Override
    public List<Purchase> findAll() {

        return purchaseRepository.findAll();
    }

    @Override
    public Optional<Purchase> findOne(Integer id) {

        return Optional.ofNullable(purchaseRepository.findOne(id));
    }

    private double getUserPercent(User user) {
        Config config = configRepository.findAll().get(0);
        switch (user.getPerson().getLevel()) {
            case 1:
                return config.getLevelOnePercent();
            case 2:
                return config.getLevelTwoPercent();
            case 3:
                return config.getLevelThreePercent();
            default:
                return 0;
        }
    }

    @Override
    @Transactional
    public double purchase(final PurchaseRequest request) throws NoMoreTicketsException{

        User user = userRepository.getOne(request.getUser().getId());

        request.getPurchase().getTickets().forEach(ticket -> {
            Flight dbFlight = flightRepository.getOne(ticket.getFlight().getId());
            ticket.setFlight(dbFlight);

            int seats = dbFlight.getPlane().getSeats();
            List<Ticket> tickets = ticketRepository.findByFlight(dbFlight);
            if(tickets.size() > seats || request.getPurchase().getTickets().size() > seats){
                throw new NoMoreTicketsException();
            }
        });

        Purchase savedPurchase = purchaseRepository.save(request.getPurchase());

        double points = savedPurchase.getTickets().stream()
                .mapToDouble(value -> value.getFlight().getPoints()).sum();

        user.getPerson().addPurchase(savedPurchase);
        user.getPerson().addPoints(points * getUserPercent(user));
        User savedUser = userRepository.save(user);

        List<Flight> flightsToAdd = new ArrayList<>();
        for (Ticket ticket : savedPurchase.getTickets()) {
            Flight flight = flightRepository.getOne(ticket.getFlight().getId());
            flight.addCustomer(savedUser.getPerson());
            flightsToAdd.add(flight);
        }
        flightsToAdd.forEach(flight -> flightRepository.save(flight));

        return user.getPerson().getPoints();
    }

    @Override
    @Transactional
    public double purchaseWithPoint(PurchaseRequest request) {

        User user = userRepository.getOne(request.getUser().getId());
        Purchase savedPurchase = purchaseRepository.save(request.getPurchase());

        user.getPerson().addPurchase(savedPurchase);
        user.getPerson().setPoints(0.0);

        User savedUser = userRepository.save(user);

        List<Flight> flightsToAdd = new ArrayList<>();
        for (Ticket ticket : savedPurchase.getTickets()) {
            Flight flight = flightRepository.getOne(ticket.getFlight().getId());
            flight.addCustomer(savedUser.getPerson());
            flightsToAdd.add(flight);
        }
        flightsToAdd.forEach(flight -> flightRepository.save(flight));

        return 0.0;
    }
}
