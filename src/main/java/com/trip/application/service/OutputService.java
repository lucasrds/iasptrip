package com.trip.application.service;

import java.util.List;
import java.util.Optional;

public interface OutputService<T, ID> {

    List<T> findAll();

    Optional<T> findOne(ID id);
}
