package com.trip.application.service.address.impl;

import com.trip.application.repository.address.AddressRepository;
import com.trip.application.service.address.AddressService;
import com.trip.application.entity.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    private AddressRepository addressRepository;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository) {

        this.addressRepository = addressRepository;
    }

    @Override
    public boolean insert(Address address) {

        addressRepository.save(address);
        return true;
    }

    @Override
    public boolean update(Address address) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        addressRepository.delete(id);
        return true;
    }

    @Override
    public List<Address> findAll() {

        return addressRepository.findAll();
    }

    @Override
    public Optional<Address> findOne(Integer id) {

        return Optional.ofNullable(addressRepository.findOne(id));
    }
}
