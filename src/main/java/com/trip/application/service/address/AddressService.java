package com.trip.application.service.address;

import com.trip.application.service.ServiceBehavior;
import com.trip.application.entity.Address;

public interface AddressService extends ServiceBehavior<Address, Integer> {

}
