package com.trip.application.service.route;

import com.trip.application.service.ServiceBehavior;
import com.trip.application.entity.Route;

public interface RouteService extends ServiceBehavior<Route, Integer> {

}
