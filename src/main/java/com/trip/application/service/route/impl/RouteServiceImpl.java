package com.trip.application.service.route.impl;

import com.trip.application.repository.route.RouteRepository;
import com.trip.application.service.route.RouteService;
import com.trip.application.entity.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RouteServiceImpl implements RouteService {

    private RouteRepository routeRepository;

    @Autowired
    public RouteServiceImpl(RouteRepository routeRepository) {

        this.routeRepository = routeRepository;
    }

    @Override
    public boolean insert(Route route) {

        routeRepository.save(route);
        return true;
    }

    @Override
    public boolean update(Route route) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        routeRepository.delete(id);
        return true;
    }

    @Override
    public List<Route> findAll() {

        return routeRepository.findAll();
    }

    @Override
    public Optional<Route> findOne(Integer id) {

        return Optional.ofNullable(routeRepository.findOne(id));
    }
}
