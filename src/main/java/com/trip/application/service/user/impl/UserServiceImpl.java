package com.trip.application.service.user.impl;

import com.trip.application.entity.User;
import com.trip.application.repository.user.UserRepository;
import com.trip.application.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {

        this.userRepository = userRepository;
    }

    @Override
    public boolean insert(User user) {

        userRepository.save(user);
        return true;
    }

    @Override
    public boolean update(User user) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        userRepository.delete(id);
        return true;
    }

    @Override
    public List<User> findAll() {

        return userRepository.findAll();
    }

    @Override
    public Optional<User> findOne(Integer id) {

        return Optional.ofNullable(userRepository.findOne(id));
    }

    @Override
    public User login(User user) {

        return userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
    }
}
