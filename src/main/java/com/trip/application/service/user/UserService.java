package com.trip.application.service.user;

import com.trip.application.entity.User;
import com.trip.application.service.ServiceBehavior;

public interface UserService extends ServiceBehavior<User, Integer> {
    User login(User user);
}
