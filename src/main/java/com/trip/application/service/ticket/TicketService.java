package com.trip.application.service.ticket;

import com.trip.application.service.ServiceBehavior;
import com.trip.application.entity.Ticket;

public interface TicketService extends ServiceBehavior<Ticket, Integer> {

}
