package com.trip.application.service.ticket.impl;

import com.trip.application.repository.ticket.TicketRepository;
import com.trip.application.service.ticket.TicketService;
import com.trip.application.entity.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TicketServiceImpl implements TicketService {

    private TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {

        this.ticketRepository = ticketRepository;
    }

    @Override
    public boolean insert(Ticket ticket) {

        ticketRepository.save(ticket);
        return true;
    }

    @Override
    public boolean update(Ticket ticket) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        ticketRepository.delete(id);
        return true;
    }

    @Override
    public List<Ticket> findAll() {

        return ticketRepository.findAll();
    }

    @Override
    public Optional<Ticket> findOne(Integer id) {

        return Optional.ofNullable(ticketRepository.findOne(id));
    }
}
