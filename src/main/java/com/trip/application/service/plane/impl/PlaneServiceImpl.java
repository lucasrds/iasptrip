package com.trip.application.service.plane.impl;

import com.trip.application.repository.plane.PlaneRepository;
import com.trip.application.service.plane.PlaneService;
import com.trip.application.entity.Plane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlaneServiceImpl implements PlaneService {

    private PlaneRepository planeRepository;

    @Autowired
    public PlaneServiceImpl(PlaneRepository planeRepository) {

        this.planeRepository = planeRepository;
    }

    @Override
    public boolean insert(Plane plane) {

        planeRepository.save(plane);
        return true;
    }

    @Override
    public boolean update(Plane plane) {

        return false;
    }

    @Override
    public boolean delete(Integer id) {

        planeRepository.delete(id);
        return true;
    }

    @Override
    public List<Plane> findAll() {

        return planeRepository.findAll();
    }

    @Override
    public Optional<Plane> findOne(Integer id) {

        return Optional.ofNullable(planeRepository.findOne(id));
    }
}
