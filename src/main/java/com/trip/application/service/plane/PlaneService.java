package com.trip.application.service.plane;

import com.trip.application.service.ServiceBehavior;
import com.trip.application.entity.Plane;

public interface PlaneService extends ServiceBehavior<Plane, Integer> {

}
